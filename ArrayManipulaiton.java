package lab1;
public class ArrayManipulaiton {
    public static void main(String[] args) {
        int[] numbers =  {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        int sum = 0;
        double max =0;
        System.out.print("numbers : ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
            sum+=numbers[i];
        }
        System.out.println();
        System.out.print("names : ");
        for (String i : names) {
            System.out.print(i+" ");
        }
        System.out.println();
        values[0] = 1.5;
        values[1] = 3.5;
        values[2] = 2.0;
        values[3] = 5.0;
        System.out.print("sum numbers = "+sum);
        System.out.println();
        for (int i = 0; i < values.length; i++) {
            if(values[i]>max){
                max = values[i];
            }
        }
        System.out.print("max values = "+max);
        System.out.println();
        String[] reversedNames = new String[names.length];
        for (int i = 0,j=names.length-1; i < reversedNames.length; i++,j--) {
            reversedNames[i]=names[j];
        }
        System.out.print("reversedNames : ");
        for (String string : reversedNames) {
            System.out.print(string+" ");
        }
        System.out.println();
        int keep =0;
        for (int j = 0; j < numbers.length; j++) {
            for (int i = 0; i < numbers.length-1; i++) {
            if(numbers[i]>numbers[i+1]){
                keep = numbers[i];
                numbers[i]=numbers[i+1];
                numbers[i+1] = keep;
            }
            
        }
        }
        System.out.print("number sort : ");
        for (int j : numbers) {
            System.out.print(j+" ");
        }
    }
}
